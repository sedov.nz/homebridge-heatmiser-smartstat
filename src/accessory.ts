import {
  API,
  Logging,
  Service,
  Characteristic,
  AccessoryConfig,
  AccessoryPlugin
} from 'homebridge'
import https from 'https'

module.exports = (api: API) => {
  api.registerAccessory('Heatmiser SmartStat', HeatmiserSmartStat)
}

class HeatmiserSmartStat implements AccessoryPlugin {
  private readonly Service: typeof Service = this.api.hap.Service
  private readonly Characteristic: typeof Characteristic = this.api.hap.Characteristic

  private token = ''
  private device = ''
  private deviceId = ''
  private deviceId2 = ''

  private currentTemperature = 20
  private currentSetTemperature = 20
  private standby = false

  private targetState: number = this.Characteristic.TargetHeatingCoolingState.OFF

  private readonly options = {
    agent: new https.Agent({
      keepAlive: true
    }),
    hostname: 'neohub.co.uk',
    port: 443,
    method: 'POST',
    timeout: 3000
  }

  constructor (
    private readonly log: Logging,
    private readonly config: AccessoryConfig,
    private readonly api: API
  ) {
    this.log = log
    this.config = config
    this.api = api

    // Получаем токен
    this.userLogin()

    // Запрашиваем инфорацию каждые 30 секунд
    setInterval(() => {
      this.deviceStatus()
    }, 30000)
  }

  userLogin () {
    const post = new URLSearchParams({
      USERNAME: this.config.username,
      PASSWORD: this.config.password,
      devicetypeid: '2'
    }).toString()

    const options = {
      ...this.options,
      ...{
        path: '/hm_user_login',
        headers: { 'Content-Length': post.length }
      }
    }

    const req = https.request(options, res => {
      let response = ''

      res.setEncoding('utf8')

      res.on('data', data => {
        response += data
      })

      res.on('end', () => {
        try {
          const z = JSON.parse(response)

          if ('STATUS' in z && z.STATUS === 1) {
            this.token = z.TOKEN

            if ('devices' in z && z.devices.length > 0) {
              this.deviceId = z.devices[0].deviceid

              // Запрашиваем актуальные данные
              this.deviceStatus()
            } else {
              this.log.error('No devices found')
            }
          } else {
            this.log.error('auth error: ' + response)
          }
        } catch (err) {
          this.log.error('auth error: ' + (err as Error).message + ' ' + response)
        }
      })

      res.on('error', (err) => {
        this.log.error('auth error: ' + err.toString())
      })
    })

    req.on('error', (err) => {
      this.log.error('auth error: ' + err.toString())
    })

    req.write(post)
    req.end()
  }

  deviceStatus () {
    const post = new URLSearchParams({
      device_id: this.deviceId,
      token: this.token
    }).toString()

    const options = {
      ...this.options,
      ...{
        path: '/hm_device_status',
        headers: { 'Content-Length': post.length }
      }
    }

    const req = https.request(options, res => {
      let response = ''

      res.setEncoding('utf8')

      res.on('data', data => {
        response += data
      })

      res.on('end', () => {
        try {
          const z = JSON.parse(response)

          // Видимо, протух токен
          if ('STATUS' in z && z.STATUS === 401) {
            this.userLogin()
          }

          if ('devices' in z && z.devices.length > 0) {
            this.device = z.devices[0].device
            this.deviceId2 = z.devices[0].deviceid
            this.currentTemperature = z.devices[0].CURRENT_TEMPERATURE
            this.currentSetTemperature = z.devices[0].CURRENT_SET_TEMPERATURE
            this.standby = z.devices[0].STANDBY

            this.targetState = this.standby
              ? this.Characteristic.CurrentHeatingCoolingState.OFF
              : this.Characteristic.CurrentHeatingCoolingState.HEAT
          }
        } catch (err) {
          this.log.error('status: ' + (err as Error).message + ' ' + response)
        }
      })

      res.on('error', (err) => {
        this.log.error('status: ' + err.toString())
      })
    })

    req.on('error', err => {
      this.log.error('status: ' + err.toString())
    })

    req.write(post)
    req.end()
  }

  addCommand (command: string) {
    const post = new URLSearchParams({
      command,
      device_id: this.deviceId2,
      token: this.token
    }).toString()

    const options = {
      ...this.options,
      ...{
        path: 'https://neohub.co.uk/hm_add_command',
        headers: { 'Content-Length': post.length }
      }
    }

    const req = https.request(options, res => {
      let response = ''

      res.setEncoding('utf8')

      res.on('data', data => {
        response += data
      })

      res.on('end', () => {
        try {
          const z = JSON.parse(response)

          // @TODO: repeat request
          if ('STATUS' in z && z.STATUS === 401) {
            this.log.error('command: expired token')
            this.userLogin()
          }

          if ('COMMANDID' in z) {
            const commandId = z.COMMANDID
            this.log.debug('commandId: ' + commandId)
          }
        } catch (err) {
          this.log.error((err as Error).message, response)
        }
      })

      res.on('error', (err) => {
        this.log.error('command: ' + err.toString())
      })
    })

    req.on('error', err => {
      this.log.error('command: ' + err.toString())
    })

    req.write(post)
    req.end()
  }

  /*
   * This method is called directly after creation of this instance.
   * It should return all services which should be added to the accessory.
   */
  getServices (): Service[] {
    const informationService = new this.Service.AccessoryInformation()

    informationService
      .setCharacteristic(this.Characteristic.Manufacturer, 'Heatmiser')
      .setCharacteristic(this.Characteristic.Model, 'SmartStat')
      // .setCharacteristic(Characteristic.SerialNumber, '')
      // .setCharacteristic(Characteristic.FirmwareRevision, '')

    const thermostatService = new this.Service.Thermostat('Heatmiser SmartStat')

    thermostatService.getCharacteristic(this.Characteristic.CurrentHeatingCoolingState)
      .onGet(this.handleCurrentHeatingCoolingStateGet.bind(this))

    thermostatService.getCharacteristic(this.Characteristic.TargetHeatingCoolingState)
      .setProps({
        maxValue: this.Characteristic.TargetHeatingCoolingState.HEAT
      })
      .onGet(this.handleTargetHeatingCoolingStateGet.bind(this))
      .onSet(this.handleTargetHeatingCoolingStateSet.bind(this))

    thermostatService.getCharacteristic(this.Characteristic.CurrentTemperature)
      .onGet(this.handleCurrentTemperatureGet.bind(this))

    thermostatService.getCharacteristic(this.Characteristic.TargetTemperature)
      .setProps({
        minValue: 5,
        maxValue: 35,
        minStep: 1
      })
      .onGet(this.handleTargetTemperatureGet.bind(this))
      .onSet(this.handleTargetTemperatureSet.bind(this))

    thermostatService.getCharacteristic(this.Characteristic.TemperatureDisplayUnits)
      .onGet(this.handleTemperatureDisplayUnitsGet.bind(this))
      .onSet(this.handleTemperatureDisplayUnitsSet.bind(this))

    thermostatService.getCharacteristic(this.Characteristic.Name)
      .onGet(this.handleNameGet.bind(this))

    return [
      informationService,
      thermostatService
    ]
  }

  handleNameGet () {
    return 'Heatmiser SmartStat'
  }

  /**
   * Handle requests to get the current value of the "Current Heating Cooling State" characteristic
   */
  handleCurrentHeatingCoolingStateGet () {
    return this.standby
      ? this.Characteristic.CurrentHeatingCoolingState.OFF
      : this.Characteristic.CurrentHeatingCoolingState.HEAT
  }

  /**
   * Handle requests to get the current value of the "Target Heating Cooling State" characteristic
   */
  handleTargetHeatingCoolingStateGet () {
    return this.targetState
  }

  /**
   * Handle requests to set the "Target Heating Cooling State" characteristic
   */
  handleTargetHeatingCoolingStateSet (value) {
    this.targetState = (value === this.Characteristic.CurrentHeatingCoolingState.OFF)
      ? this.Characteristic.CurrentHeatingCoolingState.OFF
      : this.Characteristic.CurrentHeatingCoolingState.HEAT

    let command = `{'FROST_OFF':['${this.device}']}`

    if (this.targetState === this.Characteristic.CurrentHeatingCoolingState.OFF) {
      command = `{'FROST_ON':['${this.device}']}`
    }

    this.addCommand(command)
  }

  /**
   * Handle requests to get the current value of the "Current Temperature" characteristic
   */
  handleCurrentTemperatureGet () {
    return this.currentTemperature
  }

  /**
   * Handle requests to get the current value of the "Target Temperature" characteristic
   */
  handleTargetTemperatureGet () {
    return this.currentSetTemperature
  }

  /**
   * Handle requests to set the "Target Temperature" characteristic
   */
  handleTargetTemperatureSet (value) {
    const t = Math.floor(value)
    this.currentSetTemperature = t

    const command = `{'SET_TEMP':[${t},'${this.device}']}`

    this.addCommand(command)

    const command2 = '{\'SET_COMFORT_LEVELS\':[{' +
      `'sunday': {'wake':['07:00',${t}], 'leave':['10:00',${t}], 'return':['17:00',${t}], 'sleep':['22:00',${t}]}, ` +
      `'monday': {'wake':['07:00',${t}], 'leave':['10:00',${t}], 'return':['17:00',${t}], 'sleep':['22:00',${t}]}}` +
      `,'${this.device}']}`

    this.addCommand(command2)
  }

  /**
   * Handle requests to get the current value of the "Temperature Display Units" characteristic
   */
  handleTemperatureDisplayUnitsGet () {
    return this.Characteristic.TemperatureDisplayUnits.CELSIUS
  }

  /**
   * Handle requests to set the "Temperature Display Units" characteristic
   */
  handleTemperatureDisplayUnitsSet (value) {
    this.log.debug('Set DisplayUnits: ' + value)
  }
}
